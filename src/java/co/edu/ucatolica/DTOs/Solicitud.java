/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucatolica.DTOs;

import java.io.Serializable;
/**
 *
 * @author Mauricio Rodriguez
 */
public class Solicitud implements Serializable {

    private Integer idSolicitud;
    private int productoidProducto;
    private int clienteIdPersona;
    private String ingMen;
    private String ocupacion;
    private String tipoSolicitud;
    private String identificacion;

    public Solicitud() {
        super();
    }

    public Solicitud(Integer idSolicitud, int productoidProducto, int clienteIdPersona, String ingMen, String ocupacion, String tipoSolicitud) {
        super();
        this.idSolicitud = idSolicitud;
        this.productoidProducto = productoidProducto;
        this.clienteIdPersona = clienteIdPersona;
        this.ingMen = ingMen;
        this.ocupacion = ocupacion;
        this.tipoSolicitud = tipoSolicitud;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public int getProductoidProducto() {
        return productoidProducto;
    }

    public void setProductoidProducto(int productoidProducto) {
        this.productoidProducto = productoidProducto;
    }

    public int getClienteIdPersona() {
        return clienteIdPersona;
    }

    public void setClienteIdPersona(int clienteIdPersona) {
        this.clienteIdPersona = clienteIdPersona;
    }

    public String getIngMen() {
        return ingMen;
    }

    public void setIngMen(String ingMen) {
        this.ingMen = ingMen;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }
    
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
}
