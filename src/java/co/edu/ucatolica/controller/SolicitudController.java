/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucatolica.controller;


import co.edu.ucatolica.DAOs.SolicitudDAO;
import co.edu.ucatolica.DTOs.Persona;
import co.edu.ucatolica.DTOs.Solicitud;
import co.edu.ucatolica.bds.MySqlDataSource;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author camila
 */
@Controller
@RequestMapping("/")
public class SolicitudController {
    
    
    
      @RequestMapping("SolicitudCrear.htm")
    public String processSubmit2crear(HttpServletRequest req,SessionStatus status,ModelMap model) 
    {
        
        SolicitudDAO pDao = new SolicitudDAO();
        Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");       
        Persona p = new Persona();
         List<Persona> datos= pDao.consultarCliente( MySqlDataSource.getConexionBD());
       // List<ClienteDTO> datos =p.getDatos();
        //Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        if (datos.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datos.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "Solicitud_crear";
    }
    //---------------------------------------------------------------------------------------------------------
        @RequestMapping(method = RequestMethod.GET, value = "SolicitudDigita.htm")
    public String processSubmit(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        System.out.println("SolicitudCrear");
        model.put("mensajeSolicitud", "Pase por el controller de Solicitud:::"+req.getParameter("nombre"));
        return "Solicitud_digita";
    }    

@RequestMapping(method = RequestMethod.POST, value = "SolicitudRegistrar.htm")
    public String processSubmit1(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        SolicitudDAO pDao = new SolicitudDAO();
        Logger.getLogger(SolicitudController.class.getName()).log(Level.INFO, "Ejecutando processSubmit1...");
       int id = pDao.obtenerId(MySqlDataSource.getConexionBD());
        String clienteIdPersona = req.getParameter("clienteIdPersona");
        String productoidProducto = req.getParameter("productoidProducto");
        String ingMen = req.getParameter("ingMen");
        String ocupacion = req.getParameter("ocupacion");
        String tipoSolicitud = req.getParameter("tipoSolicitud");
        Persona c=new Persona();
        String identi=req.getParameter("identificacion");

        Solicitud p = new Solicitud();
        p.setClienteIdPersona(Integer.parseInt(clienteIdPersona));
        p.setProductoidProducto(Integer.parseInt(productoidProducto));
        p.setIdSolicitud(id);
        p.setIngMen(ingMen);
        p.setOcupacion(ocupacion);
        p.setTipoSolicitud(tipoSolicitud);
        p.setIdentificacion(identi);
   
        boolean insert = pDao.create(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Registrar + " + "-" + insert);
        
        if (insert)
            model.put("mensaje", "El registro fue creado satisfactoriamente!!!");
        else
            model.put("mensaje", "El registro NO fue creado, consulte con el administrador...");
        
        return "Solicitud_digita";
    }     
    
    

 

@RequestMapping(method = RequestMethod.GET, value = "SolicitudConsultar.htm")
    public String processSubmit2(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {      
        Logger.getLogger(SolicitudController.class.getName()).log(Level.INFO, "Ejecutando processSubmit2...");
        return "Solicitud_consultar";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "SolicitudConsultarForm.htm")
    public String processSubmit3(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        SolicitudDAO pDao = new SolicitudDAO();
        
            
        Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");

        int id = pDao.obtenerId(MySqlDataSource.getConexionBD());
        String ident = req.getParameter("identificacion");
        String nombre1 = req.getParameter("nombre1");
        
        Solicitud p = new Solicitud();
        p.setIdSolicitud(id);
        p.setIdentificacion(ident);
        
            
        List<Solicitud> datos = pDao.consultarSolicitud(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + ident + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        if (datos.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datos.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "Solicitud_consultar";
    }     
    
@RequestMapping(method = RequestMethod.GET, value = "SolicitudEditar.htm")
    public String processSubmit4(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {      
        Logger.getLogger(SolicitudController.class.getName()).log(Level.INFO, "Ejecutando processSubmit4...");
        return "Solicitud_editar";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "SolicitudEditarForm1.htm")
    public String processSubmit5(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

       SolicitudDAO pDao = new SolicitudDAO();
        
        Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit5...");

        
        String ident = req.getParameter("identificacion");
        String tipoSolicitud = req.getParameter("tipoSolicitud");
        int id = pDao.obtenerIdS(tipoSolicitud,ident,MySqlDataSource.getConexionBD()); 
        Solicitud p = new Solicitud(); 
        p.setIdSolicitud(id);
        p.setTipoSolicitud(tipoSolicitud);
        p.setIdentificacion(ident);
        List<Solicitud> datos = pDao.consultarSolicitud(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + ident + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        
        
        return "Solicitud_editar";
        
    }    
    @RequestMapping(method = RequestMethod.POST, value = "SolicitudEditarForm2.htm")
    public String processSubmit6(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

       SolicitudDAO pDao = new SolicitudDAO();
            
        Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit6...");
        String var = req.getParameter("tipo");
        String ident = req.getParameter("identificacion");
        String tipoSolicitud = req.getParameter("tipoSolicitud");
        int id = pDao.obtenerIdS(var,ident,MySqlDataSource.getConexionBD()); 
        Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Id Solicitud: " + id +""+tipoSolicitud+" "+var);
        
       Solicitud p = new Solicitud();
       p.setIdSolicitud(id);
       p.setTipoSolicitud(tipoSolicitud);
       p.setIdentificacion(ident);
  
        boolean res = pDao.editarSolicitud(p, MySqlDataSource.getConexionBD());                         
        
        if (res)
            model.put("mensaje", "Se edito satisfactoriamente!!!");
        else
            model.put("mensaje", "NO se guardaron los cambios...");
        
        return "Solicitud_editar";
        
    }  
}
