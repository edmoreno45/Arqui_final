/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucatolica.DAOs;

import co.edu.ucatolica.DTOs.Persona;
import co.edu.ucatolica.DTOs.Solicitud;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mauricio Rodriguez
 */
public class SolicitudDAO {
    public boolean create(Solicitud emc, Connection con){
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {   
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando crearSolicitud...");
            pstmt = con.prepareStatement("INSERT INTO solicitud VALUES (?,?,?,?,?,?,?)");
            
            pstmt.setInt(1,emc.getIdSolicitud());
            pstmt.setInt(2, emc.getProductoidProducto());
            pstmt.setInt(3, emc.getClienteIdPersona());
            pstmt.setString(4, emc.getIngMen());
            pstmt.setString(5, emc.getOcupacion());
            pstmt.setString(6, emc.getTipoSolicitud());
            pstmt.setString(7, emc.getIdentificacion());
            pstmt.execute();
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;
    }
    
    public ArrayList<Solicitud> consultarSolicitud(Solicitud emc, Connection con){
        
        ArrayList<Solicitud> datos = new ArrayList<Solicitud>();
        
        Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando consultarSolicitud...");
        
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select idSolicitud, productoidProducto,clienteIdPersona, ingMen, ocupacion, tipoSolicitud, identificacion  from solicitud "
                    + " where "
                    + " idSolicitud='" + emc.getIdSolicitud()+"'");
            
            while (rs.next())
            { 
                Solicitud cta = new Solicitud();
                cta.setIdSolicitud(rs.getInt(1));
                cta.setProductoidProducto(rs.getInt(2));
                cta.setClienteIdPersona(rs.getInt(3));
                cta.setIngMen(rs.getString(4));
                cta.setOcupacion(rs.getString(5));
                cta.setTipoSolicitud(rs.getString(6));
                cta.setIdentificacion(rs.getString(7));
                datos.add(cta);
            }
            
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando consultarSolicitud fin..." + datos.size());
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
    
    public int obtenerId(Connection con)
    {
        int id = -1;
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select max(idSolicitud)+1 from solicitud");
            
            while (rs.next())
            { 
                id = rs.getInt(1);
            }

            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
    }
         
    public boolean editarSolicitud(Solicitud dtc, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO, "Ejecutando editarSolicitud...");
            
            pstmt = con.prepareStatement("UPDATE solicitud "
                    + " SET "
                    + " ingMen=?"
                    + " ,ocupacion=?"
                    + " ,tipoSolicitud=?"
                    + " ,identificacion=?"
                    + " where idSolicitud=?");
                        
            pstmt.setString(1, dtc.getIngMen());
            pstmt.setString(2, dtc.getOcupacion());
            pstmt.setString(3, dtc.getTipoSolicitud());
            pstmt.setString(4, dtc.getIdentificacion());
            pstmt.setInt(5, dtc.getIdSolicitud());
            
            pstmt.executeUpdate();
            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }    

    public ArrayList<Persona> consultarCliente(Connection con){
        
       ArrayList<Persona> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(Persona.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select identificacion, nombre_1,nombre_2, "
                    + " apellido_1, apellido_2, genero, telefono, email, "
                    + " fecha_nacimiento, tipo_persona, id_persona "
                    + " from persona ");
            
            while (rs.next())
            { 
               Persona per=new Persona();
                per.setIdentificacion(rs.getString(1));
                per.setNombre1(rs.getString(2));
                per.setNombre2(rs.getString(3));
                per.setApellido1(rs.getString(4));
                per.setApellido2(rs.getString(5));
                per.setGenero(rs.getString(6));
                per.setTelef(rs.getString(7));
                per.setEmail(rs.getString(8));
                per.setfNacimiento(rs.getString(9));
                per.setTipoP(rs.getString(10));
                per.setId(rs.getInt(11));
                
                datos.add(per);  
            }
            Persona per=new Persona();
            //per.setDatos(datos);
           for(int x=0;x<datos.size();x++) {
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO,per.getIdentificacion());
            }
             Logger.getLogger(SolicitudDAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
    public int obtenerIdS(String tipo,String iden,Connection con){
        int id=0;
        try {
            Statement s = con.createStatement();
             ResultSet rs = s.executeQuery ("select idSolicitud from solicitud where identificacion='"+iden+"'and tipoSolicitud='"+tipo+"' ");
            
            while (rs.next())
            { 
                id = rs.getInt(1);
            }
           

            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(SolicitudDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
    }
    
}
